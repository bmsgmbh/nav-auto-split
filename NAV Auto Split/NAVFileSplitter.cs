﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace NAV_Auto_Split
{

    class NAVFileSplitter
    {
        public string ErrorMsg;

        public NAVFileSplitter()
        {
        }

        public bool splitNAVfile(string path)
        {
            string line;

            // fuck you microsoft
            try
            {
                FileStream file = File.Open(path, FileMode.Open);
                file.Close();
            }
            catch (Exception ex)
            {
                System.Threading.Thread.Sleep(100);
                splitNAVfile(path);
                return false;
            }

            StreamReader orgFile = new StreamReader(path);
            StreamWriter writer = new StreamWriter(Path.GetTempFileName());

            try
            {
                while ((line = orgFile.ReadLine()) != null)
                {
                    if (line.StartsWith("OBJECT"))
                    {
                        writer.Close();

                        writer = File.CreateText(this.getFilename(line, path));
                    }

                    writer.WriteLine(line);
                }

                writer.Close();
            }
            catch (Exception ex)
            {
                orgFile.Close();

                this.ErrorMsg = ex.Message;

                return false;
            }

            orgFile.Close();

            return true;
        }

        private string getFilename(string line, string fullPath)
        {
            string filename = Path.GetDirectoryName(fullPath) + "\\";

            string[] splittedString = line.Split(' ');

            string objType;
            string objID;

            if (splittedString.Length >= 3)
            {
                objType = splittedString[1];
                objID = splittedString[2];

                filename += objType + "_" + objID + ".txt";
            }

            return filename;
        }
    }
}
