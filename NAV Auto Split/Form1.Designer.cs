﻿namespace NAV_Auto_Split
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.pathItems_list = new System.Windows.Forms.ListBox();
            this.addItem_btn = new System.Windows.Forms.Button();
            this.deleteItem_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // pathItems_list
            // 
            this.pathItems_list.FormattingEnabled = true;
            this.pathItems_list.Location = new System.Drawing.Point(12, 12);
            this.pathItems_list.Name = "pathItems_list";
            this.pathItems_list.Size = new System.Drawing.Size(558, 134);
            this.pathItems_list.TabIndex = 0;
            // 
            // addItem_btn
            // 
            this.addItem_btn.Location = new System.Drawing.Point(495, 152);
            this.addItem_btn.Name = "addItem_btn";
            this.addItem_btn.Size = new System.Drawing.Size(75, 23);
            this.addItem_btn.TabIndex = 1;
            this.addItem_btn.Text = "Hinzufügen";
            this.addItem_btn.UseVisualStyleBackColor = true;
            this.addItem_btn.Click += new System.EventHandler(this.addItem_btn_Click);
            // 
            // deleteItem_btn
            // 
            this.deleteItem_btn.Location = new System.Drawing.Point(414, 152);
            this.deleteItem_btn.Name = "deleteItem_btn";
            this.deleteItem_btn.Size = new System.Drawing.Size(75, 23);
            this.deleteItem_btn.TabIndex = 2;
            this.deleteItem_btn.Text = "Entfernen";
            this.deleteItem_btn.UseVisualStyleBackColor = true;
            this.deleteItem_btn.Click += new System.EventHandler(this.deleteItem_btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 187);
            this.Controls.Add(this.deleteItem_btn);
            this.Controls.Add(this.addItem_btn);
            this.Controls.Add(this.pathItems_list);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox pathItems_list;
        private System.Windows.Forms.Button addItem_btn;
        private System.Windows.Forms.Button deleteItem_btn;
    }
}

