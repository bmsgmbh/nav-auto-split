﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace NAV_Auto_Split
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void addItem_btn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fileOpenDialog = new FolderBrowserDialog();

            if (fileOpenDialog.ShowDialog() == DialogResult.OK)
            {
                FileSystemWatcher watcher = new FileSystemWatcher();

                watcher.Path = fileOpenDialog.SelectedPath;
                watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.Attributes | NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.Security | NotifyFilters.Size;
                watcher.Filter = "_*.txt";

                watcher.Created += new FileSystemEventHandler(OnChanged);

                watcher.EnableRaisingEvents = true;

                pathItems_list.Items.Add(fileOpenDialog.SelectedPath);
            }
        }

        private void deleteItem_btn_Click(object sender, EventArgs e)
        {
            if (pathItems_list.SelectedIndex != -1)
            {
                pathItems_list.Items.RemoveAt(pathItems_list.SelectedIndex);
            }
        }

        private void pathItems_list_DataSourceChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Added");
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            NAVFileSplitter navFileSplitter = new NAVFileSplitter();
            navFileSplitter.splitNAVfile(e.FullPath);
        }
    }
}
